//
//  UIViewController.swift
//  ServerMedan
//
//  Created by DickyChengg on 11/10/16.
//  Copyright © 2016 DickyChengg. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func initial(_ addLeftMenu: Bool = true, _ addRightMenu: Bool = true)
    {
        DClass.controller = self;
        if(addLeftMenu)
        {
            DClass.controller.navigationItem.leftBarButtonItem = UIBarButtonItem(
                image: UIImage(named: ""),
                style: .done,
                target: DClass.controller,
                action: #selector(showMenu.left)
            );
            
            let swipeGesture = UISwipeGestureRecognizer(
                target: self,
                action: #selector(showMenu.left)
            );
            swipeGesture.direction = .right;
            DClass.controller.view.addGestureRecognizer(
                swipeGesture
            );
        }
        if(addRightMenu)
        {
           DClass.controller.navigationItem.rightBarButtonItem = UIBarButtonItem(
                image: UIImage(named: ""),
                style: .done,
                target: self,
                action: #selector(showMenu.right)
            );
            
            let swipeGesture = UISwipeGestureRecognizer(
                target: self,
                action: #selector(showMenu.right)
            );
            swipeGesture.direction = .left;
            DClass.controller.view.addGestureRecognizer(
                swipeGesture
            );
        }
    }
    
    func DSegue(storyboard: String, _ identifier: String) -> UIViewController!
    {
        let vc = UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(withIdentifier: identifier)
        return vc
    }
    
    class showMenu
    {
        @objc class func left()
        {
            DClass.controller.showFader();
            let board = DClass.controller.storyboard?.instantiateViewController(withIdentifier: "leftMenuStr") as! LeftSideMenu;
            board.view.frame.origin.x = -screen.width;
            
            DClass.controller.present(
                board,
                animated: false){
                    UIView.animate(
                        withDuration: 0.3,
                        animations: {
                            board.view.frame.origin.x = 0
                        }, completion: nil);
            };
        }
        
        @objc class func right()
        {
            DClass.controller.showFader();
            let board = DClass.controller.storyboard?.instantiateViewController(withIdentifier: "rightMenuStr") as! RightSideMenu;
            board.view.frame.origin.x = screen.width;
            
            DClass.controller.present(
                board,
                animated: true){
                    UIView.animate(
                        withDuration: 0.3,
                        animations: {
                            
                        }, completion: nil);
            };
        }
    }
    
    func showFader(_ animate: Bool = true, _ completion: ((Bool)->Void)? = nil)
    {
        fader = UIView(frame: CGRect(
            x: 0,
            y: 0,
            width: screen.width,
            height: screen.height))
        fader.backgroundColor = UIColor.init(red: 0/256, green: 0/256, blue: 0/256, alpha: 1)
        fader.alpha = 0
        if animate {
            let activity = UIActivityIndicatorView(frame: self.view.frame)
            activity.activityIndicatorViewStyle = .whiteLarge
            activity.startAnimating()
            activity.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
            activity.center = view.center
            fader.addSubview(activity)
            activity.startAnimating()
        }
        self.view.addSubview(fader)
        UIView.animate(
            withDuration: 0.1,
            delay: 0,
            options: .curveEaseInOut,
            animations: {
                fader.alpha = 0.6
            },
            completion: completion
        )
    }
    
    
    func hideFader()
    {
        if fader != nil
        {
            UIView.animate(
                withDuration: 0.3,
                delay: 0,
                options: [.curveEaseInOut, .showHideTransitionViews],
                animations: {
                    fader.alpha = 0;
            }){
                _ in
                fader.removeFromSuperview();
                fader = nil;
            };
        }
    }
    
    func rgba(_ rgba: [CGFloat], _ alpha: CGFloat = 1) -> UIColor
    {
        if(rgba.count != 3)
        {
            return UIColor.clear;
        }
        return UIColor.init(
            red: rgba[0] / 256,
            green: rgba[1] / 256,
            blue: rgba[2] / 256,
            alpha: alpha
        );
    }
    
    func Alert(message: String, title: String, actions: [UIAlertAction] = [])
    {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )
        for action in actions
        {
            alert.addAction(action)
        }
        if(actions == [])
        {
            alert.addAction(
                UIAlertAction(
                    title: "OK",
                    style: .cancel,
                    handler: nil)
            )
        }
        self.present(alert, animated: true, completion: nil)
    }
}
