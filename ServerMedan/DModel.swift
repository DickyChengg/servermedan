//
//  DModel.swift
//  ServerMedan
//
//  Created by DickyChengg on 11/17/16.
//  Copyright © 2016 DickyChengg. All rights reserved.
//

import UIKit

class DModel {
    
    class filePath
    {
        class func bundle(_ fileName: String, _ extensions: String) -> String
        {
            return Bundle.main.path(forResource: fileName, ofType: extensions) ?? ""
        }
        
        class func directory(_ fileName: String) -> String
        {
            let documentsDirectory:NSString = NSSearchPathForDirectoriesInDomains(
                .documentDirectory,
                .userDomainMask,
                true
                )[0] as String as NSString
            let url = NSURL(fileURLWithPath: documentsDirectory as String)
            return url.appendingPathComponent(fileName)?.path ?? ""
        }
        
        class func directoryURL(_ fileName: String) -> URL
        {
            return URL(string: "file://" + directory(fileName))!
        }
    }
    
    class file
    {
        
        class func move(_ source: URL, _ destination: URL) -> Bool
        {
            if directory.exist(destination.absoluteString)
            {
                return false
            }
            do {
                try FileManager.default.copyItem(at: source, to: DModel.filePath.directoryURL(destination.absoluteString))
                return true
            }
            catch let error{
                print("error copying file \(source) : \(error)")
                return false
            }
        }
        
        // just fill the name in the bundle
        class func move(_ source: String, _ destination: String) -> Bool
        {
            if directory.exist(destination)
            {
                return false
            }
            do {
                try FileManager.default.copyItem(
                    atPath: source,
                    toPath: DModel.filePath.directory(destination)
                )
                return true
            }
            catch let error {
                print("error copying file \(source) : \(error)")
                return false
            }
        }
        
        class bundle
        {
            class func exist(_ fileName: String) -> Bool
            {
                return DModel.file.exist(
                    DModel.filePath.bundle(
                        fileName.components(separatedBy: ".")[0],
                        fileName.components(separatedBy: ".")[1]
                    )
                )
            }
            
            class func read(_ fileName: URL) -> String
            {
                return DModel.file.read(fileName.absoluteString) ?? "nil"
            }
            
            class func read(_ fileName: String) -> String
            {
                return DModel.file.read(
                    DModel.filePath.bundle(fileName.components(separatedBy: ".")[0],
                                           fileName.components(separatedBy: ".")[1]
                    )
                    ) ?? "nil"
            }
            
            class func readJson(_ fileName: String) -> NSDictionary?
            {
                return DModel.file.readJSON(
                    DModel.filePath.bundle(fileName.components(separatedBy: ".")[0],
                                           fileName.components(separatedBy: ".")[1]
                    )
                    ) ?? NSDictionary()
            }
            
            class func write(_ fileName: URL, _ value: String) -> String
            {
                return DModel.file.write(fileName.absoluteString, value) ?? "nil"
            }
            
            class func write(_ fileName: String, _ value: String) -> String
            {
                return DModel.file.write(
                    DModel.filePath.bundle(
                        fileName.components(separatedBy: ".")[0],
                        fileName.components(separatedBy: ".")[1]
                    ),
                    value
                    ) ?? "nil"
            }
            
            class func remove(_ fileName: String) -> Bool
            {
                return DModel.file.remove(
                    DModel.filePath.bundle(
                        fileName.components(separatedBy: ".")[0],
                        fileName.components(separatedBy: ".")[1]
                    )
                )
            }
        }
        
        class directory
        {
            class func exist(_ fileName: String) -> Bool
            {
                if DModel.file.directory.read(fileName).length == 0
                {
                    _ = DModel.file.directory.remove(fileName)
                }
                
                return DModel.file.exist(
                    DModel.filePath.directory(fileName)
                )
            }
            
            class func read(_ fileName: URL) -> String
            {
                return DModel.file.read(fileName.absoluteString) ?? "nil"
            }
            
            class func read(_ fileName: String) -> String
            {
                return DModel.file.read(
                    DModel.filePath.directory(fileName)
                    ) ?? "nil"
            }
            
            class func readJson(_ fileName: String) -> NSDictionary
            {
                return DModel.file.readJSON(
                    DModel.filePath.directory(fileName)
                    ) ?? NSDictionary()
            }
            
            class func write(_ fileName: URL, _ value: String) -> String
            {
                return DModel.file.write(fileName.absoluteString, value) ?? "nil"
            }
            
            class func write(_ fileName: String, _ value: String) -> String
            {
                return DModel.file.write(
                    DModel.filePath.directory(fileName),
                    value
                    ) ?? "nil"
            }
            
            class func remove(_ fileName: String) -> Bool
            {
                return DModel.file.remove(
                    DModel.filePath.directory(fileName)
                )
            }
        }
        
        class func exist(_ fileName: String) -> Bool
        {
            return FileManager.default.fileExists(atPath: fileName)
        }
        
        class func read(_ filePath: String) -> String?
        {
            do {
                return try String(
                    contentsOf: URL(string: filePath)!,
                    encoding: String.Encoding.utf8
                )
            }
            catch {
                return nil
            }
        }
        
        class func read(_ filePath: URL) -> String?
        {
            do {
                return try String(
                    contentsOf: filePath,
                    encoding: String.Encoding.utf8)
            }
            catch {
                return nil
            }
        }
        
        class func readJSON(_ filePath: String) -> NSDictionary?
        {
            let data = NSData(contentsOfFile: filePath)
            do{
                return try JSONSerialization.jsonObject(with: data! as Data, options: [JSONSerialization.ReadingOptions.mutableContainers, JSONSerialization.ReadingOptions.allowFragments]) as? NSDictionary
            }
            catch
            {
                return NSDictionary()
            }
        }
        
        private class func write(_ filePath: String, _ text: String) -> String?
        {
            do {
                try text.write(
                    to: URL(string: filePath)!,
                    atomically: false,
                    encoding: String.Encoding.utf8
                )
                return read(filePath)
            }
            catch {
                return nil
            }
        }
        
        private class func remove(_ fileName: String) -> Bool
        {
            if !exist(fileName)
            {
                return false
            }
            do {
                try FileManager.default.removeItem(
                    atPath: fileName
                )
                return true
            }
            catch let error {
                print("error removing file \(fileName) : \(error)")
                return false
            }
        }
    }
}
