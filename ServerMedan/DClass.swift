//
//  DClass.swift
//  ServerMedan
//
//  Created by DickyChengg on 11/10/16.
//  Copyright © 2016 DickyChengg. All rights reserved.
//

import UIKit

var fader: UIView!
var screen = UIScreen.main.bounds;

internal class DClass
{
    static var AccessKey: String!
    
    // user info
    static var id: String!
    static var name: String!
    static var email: String!
    static var userLevel: String!
    static var phone: String!
    
    // address
    static var lat: String!
    static var lng: String!
    static var address: String!
    static var postal: String!
    
    
    // System Config
    static var controller: UIViewController = UIViewController()
    static var connectionStatus: Bool = false
    
    class func saveKeychain(data: NSDictionary)
    {
        if(data.count == 0)
        {
            return
        }
        let fullname = (data["fname"] as? String ?? "") + " " + (data["lname"] as? String ?? "")
        Keychain.save("SMid", data: data["id"] as? NSString ?? NSString())
        Keychain.save("SMfullname", data: fullname as NSString)
        Keychain.save("SMemail", data: data["email"] as? NSString ?? NSString())
        Keychain.save("SMaddress", data: data["address"] as? NSString ?? NSString())
        Keychain.save("SMpostal", data: data["postal"] as? NSString ?? NSString())
        Keychain.save("SMphone", data: data["phone_number"] as? NSString ?? NSString())
        Keychain.save("SMlevel", data: data["user_level"] as? NSString ?? NSString())
        Keychain.save("SMlat", data: data["lat"] as? NSString ?? NSString())
        Keychain.save("SMlng", data: data["lng"] as? NSString ?? NSString())
        printKeychain()
    }
    
    class func loadKeychain()
    {
        self.id = Keychain.loadKey("SMid") as? String ?? ""
        self.name = Keychain.loadKey("SMfullname") as? String ?? ""
        self.email = Keychain.loadKey("SMemail") as? String ?? ""
        self.userLevel = Keychain.loadKey("SMlevel") as? String ?? ""
        self.phone = Keychain.loadKey("SMphone") as? String ?? ""
        self.address = Keychain.loadKey("SMaddress") as? String ?? ""
        self.postal = Keychain.loadKey("SMpostal") as? String ?? ""
        self.lat = Keychain.loadKey("SMlat") as? String ?? ""
        self.lng = Keychain.loadKey("SMlng") as? String ?? ""
        printKeychain()
    }
    
    class func printKeychain()
    {
        print("id = \(self.id)")
        print("level = \(self.userLevel)")
        print("email = \(self.email)")
        print("fullname = \(self.name)")
        print("address = \(self.address)")
        print("postal = \(self.postal)")
        print("phone = \(self.phone)")
        print("lat = \(self.lat)")
        print("long = \(self.lng)")
    }
    
}
