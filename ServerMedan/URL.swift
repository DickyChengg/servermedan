//
//  URL.swift
//  ServerMedan
//
//  Created by DickyChengg on 11/14/16.
//  Copyright © 2016 DickyChengg. All rights reserved.
//

import UIKit
internal enum URI: String
{
    case user = "user"
    case repairman = "repairman"
    case google = "google"
    case facebook = "facebook"
}

internal func API(_ link: URI) -> String{
    return "http://192.168.43.165/servermedan/API/" + link.rawValue
}
