//
//  InformationCls.swift
//  ServerMedan
//
//  Created by DickyChengg on 11/14/16.
//  Copyright © 2016 DickyChengg. All rights reserved.
//

import UIKit

class InformationCls: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    // TextField Controller
    var activeField: UITextField!
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        self.activeField = nil
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.activeField = textField
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextField = textField.superview?.viewWithTag(textField.tag + 1) as UIResponder!
        if nextField != nil {
            nextField!.becomeFirstResponder()
        }
        else {
            textField.resignFirstResponder()
        }
        
        if textField.returnKeyType == .go
        {
            //            login(textField)
        }
        return false
    }
    
    func keyboard()
    {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardDidShow(_:)),
            name: NSNotification.Name.UIKeyboardDidShow,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillBeHidden(_:)),
            name: NSNotification.Name.UIKeyboardWillHide,
            object: nil
        )
    }
    
    func keyboardDidShow(_ notification: NSNotification)
    {
        if let activeField = self.activeField, let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0.0)
            self.scrollView.contentInset = contentInsets
            self.scrollView.scrollIndicatorInsets = contentInsets
            var aRect = self.view.frame
            aRect.size.height -= keyboardSize.size.height
            
            if (!aRect.contains(activeField.frame.origin))
            {
                self.scrollView.scrollRectToVisible(activeField.frame, animated: false)
            }
        }
    }
    
    func keyboardWillBeHidden(_ notification: NSNotification)
    {
        let contentInsets = UIEdgeInsets()
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
}
