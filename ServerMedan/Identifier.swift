//
//  Identifier.swift
//  ServerMedan
//
//  Created by DickyChengg on 11/15/16.
//  Copyright © 2016 DickyChengg. All rights reserved.
//

import UIKit

internal enum MAIN: String
{
    case sideMenu = "0248sideMenuStr"
    case information = "0248informationStr"
    
    case login = "0248loginStr"
    case register = "0248registerStr"
    case forgotPassword = "0248forgotPasswordStr"
    case resetPassword = "0248resetPasswordStr"
    
    case dateTime = "0248dateTimeStr"
    case pickerView = "0248pickerStr"
    
}

internal enum USER: String
{
    case profile = "2052ProfileStr"
    case progressHistory = "2052Protery"
}

internal enum MEMBER: String
{
    case profile = "2028ProfileStr"
}
