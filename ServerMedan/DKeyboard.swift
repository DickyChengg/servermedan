//
//  DKeyboard.swift
//  ServerMedan
//
//  Created by DickyChengg on 1/8/17.
//  Copyright © 2017 DickyChengg. All rights reserved.
//

import UIKit

class DKeyboard : UIViewController, UITextFieldDelegate
{
    var ACTIVEFIELD: UITextField!
    var SCROLLVIEW: UIScrollView!
    
    func keyboard(scrollView: UIScrollView, hideKeyboard: Bool = false)
    {
        self.SCROLLVIEW = scrollView
        self.keyboard()
        (hideKeyboard ? dismissKeyboard() : ())
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        self.ACTIVEFIELD = nil
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.ACTIVEFIELD = textField
    }
    
    func dismissKeyboard()
    {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(KBHitOutsideView))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc private func KBHitOutsideView()
    {
        view.endEditing(true)
    }
    
    private func keyboard()
    {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardDidShow(_:)),
            name: NSNotification.Name.UIKeyboardDidShow,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillBeHidden(_:)),
            name: NSNotification.Name.UIKeyboardWillHide,
            object: nil
        )
    }
    
    @objc private func keyboardDidShow(_ notification: NSNotification)
    {
        if let activeField = self.ACTIVEFIELD, let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0.0)
            self.SCROLLVIEW.contentInset = contentInsets
            self.SCROLLVIEW.scrollIndicatorInsets = contentInsets
            var aRect = self.view.frame
            aRect.size.height -= keyboardSize.size.height
            
            if (!aRect.contains(activeField.frame.origin))
            {
                self.SCROLLVIEW.scrollRectToVisible(activeField.frame, animated: false)
            }
        }
    }
    
    @objc private func keyboardWillBeHidden(_ notification: NSNotification)
    {
        let contentInsets = UIEdgeInsets()
        self.SCROLLVIEW.contentInset = contentInsets
        self.SCROLLVIEW.scrollIndicatorInsets = contentInsets
    }
}
