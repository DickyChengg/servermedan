//
//  ForgotPasswordCls.swift
//  ServerMedan
//
//  Created by DickyChengg on 11/14/16.
//  Copyright © 2016 DickyChengg. All rights reserved.
//

import UIKit

class ForgotPasswordCls: DKeyboard {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var email: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        keyboard(scrollView: scrollView, hideKeyboard: true)
    }
    
    @IBAction func send(sender: AnyObject)
    {
        if String.empty(email.text)
        {
            Alert(message: "Email tidak boleh kosong", title: "Proses Gagal")
            return
        }
        if !(email.text ?? "").isEmail
        {
            Alert(message: "Email yang anda masukkan tidak valid", title: "Proses Gagal")
            return
        }
        self.showFader()
        PostData(API(.user) + "/forgot_password", Params: "email=\(email.text!)") {
            (response: NSDictionary?, error: NSString?) in
            print(response)
            if error == nil
            {
                if response?["status"] as? Bool ?? false
                {
                    self.Alert(
                        message: "Link pemulihan kata sandi sudah dikirimkan ke email anda.",
                        title: "Berhasil",
                        actions: [
                            UIAlertAction(
                                title: "Kembali ke Login",
                                style: .default,
                                handler: {
                                    _ in
                                    self.dismiss(animated: true, completion: nil)
                                }
                            )
                        ]
                    )
                }
                else
                {
                    self.Alert(message: response?["data"] as? String ?? "", title: "Gagal")
                }
            }
            else
            {
                self.Alert(message: "Terdapat kesalahan ketika memproses data.\nCobalah beberapa saat lagi", title: "Gagal")
            }
            self.hideFader()
        }
    }
    
    @IBAction func login(sender: AnyObject)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
