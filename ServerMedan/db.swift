//
//  db.swift
//  ServerMedan
//
//  Created by DickyChengg on 11/17/16.
//  Copyright © 2016 DickyChengg. All rights reserved.
//

import UIKit

internal enum STATUS: String
{
    case finish = "0"         // the job has done
    case onProgress = "1"    // the repairman already accept
    case request = "2"        // wait for repairman response
    case action = "3"         // wait for customer response
}

class db {
    
    class func progress()
    {
        
    }
    
    class func history()
    {
        
    }
    
    class func status(_ status: STATUS)
    {
        
    }
    
}
