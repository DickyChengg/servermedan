//
//  UIButton.swift
//  ServerMedan
//
//  Created by DickyChengg on 11/10/16.
//  Copyright © 2016 DickyChengg. All rights reserved.
//

import UIKit

extension UIButton {
    
    func customButton(_ cornerRadius: CGFloat = 8, borderColor: UIColor? = nil, borderWidth: CGFloat? = 0, shadowColor: UIColor? = nil, shadowOpacity: Float? = 0.5, shadowRadius: CGFloat? = 5)
    {
        self.layer.cornerRadius = cornerRadius
        
        if borderColor != nil {
            self.layer.borderColor = borderColor!.cgColor
            self.layer.borderWidth = borderWidth!
        }
        
        if shadowColor != nil {
            self.layer.shadowColor = shadowColor!.cgColor
            self.layer.opacity = shadowOpacity!
            self.layer.shadowRadius = shadowRadius!
        }
    }
    
    private func actionHandleBlock(action:(() -> Void)? = nil) {
        struct __ {
            static var action :(() -> Void)?
        }
        if action != nil {
            __.action = action
        } else {
            __.action?()
        }
    }
    
    @objc private func triggerActionHandleBlock() {
        self.actionHandleBlock()
    }
    
    func actionHandle(controlEvents control :UIControlEvents, ForAction action:@escaping () -> Void) {
        self.actionHandleBlock(action: action)
        self.addTarget(self, action: #selector(self.triggerActionHandleBlock), for: control)
    }
}
