//
//  TableViewController.swift
//  ServerMedan
//
//  Created by DickyChengg on 11/18/16.
//  Copyright © 2016 DickyChengg. All rights reserved.
//

import UIKit

class TableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var table: DTable!
    var data: [[String : AnyObject]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table.viewDidLoad(self)
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        table.viewWillAppear(self)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        table.viewWillDisappear(self)
    }
    
    
    func requestData(_ link: String)
    {
        GetData(link) {
            (response: NSDictionary?, error: NSString?) in
            if error == nil
            {
                if (response?["status"] as? Bool ?? false) == true
                {
                    self.table.request(response?["data"] as? NSDictionary ?? NSDictionary())
                }
                else
                {
                    print("response = \(response)")
                }
            }
            else
            {
                print("error = \(error)")
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return table.numberOfSections(self, data)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return table.numberOfRows(self, data)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return table.cellForRow(self, indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        table.didSelectRow(self, indexPath)
    }
    
}
