//
//  DController.swift
//  ServerMedan
//
//  Created by DickyChengg on 11/18/16.
//  Copyright © 2016 DickyChengg. All rights reserved.
//

import UIKit

class DTable
{
    var vc: TableViewController!
    var request: ((NSDictionary)->Void)!
    var viewDidLoad: ((TableViewController)->Void)!
    var viewWillAppear: ((TableViewController)->Void)!
    var viewWillDisappear: ((TableViewController)->Void)!
    
    var cellForRow: ((TableViewController, IndexPath) -> UITableViewCell)!
    var didSelectRow: ((TableViewController, IndexPath) -> Void)!
    var numberOfRows: ((TableViewController, [[String : AnyObject]])->Int)!
    var numberOfSections: ((TableViewController, [[String : AnyObject]])->Int)!
    
    init()
    {
        
    }
    
    init(_ link: String)
    {
        vc = DClass.controller.storyboard?.instantiateViewController(withIdentifier: "tableViewStr") as! TableViewController
        if !String.empty(link)
        {
            vc.requestData(link)
        }
    }
    
    func assign(_ view: UIView)
    {
        
    }
    
    func assign(_ type: Int, _ animated: Bool = true)
    {
        if type == 0
        {
            // present ListController
            DClass.controller.present(vc, animated: animated, completion: nil)
            return
        }
        else if type == 1
        {
            // push ListController
            DClass.controller.navigationController?.pushViewController(vc, animated: animated)
            return
        }
        // Replace ListController
        DClass.controller.navigationController?.setViewControllers([vc], animated: animated)
    }
    
    deinit
    {
        viewDidLoad = nil
        numberOfRows = nil
        viewWillAppear = nil
        viewWillDisappear = nil
        numberOfSections = nil
        didSelectRow = nil
        cellForRow = nil
        request = nil
        vc = nil
    }
}
