//
//  Server.swift
//  ServerMedan
//
//  Created by DickyChengg on 11/14/16.
//  Copyright © 2016 DickyChengg. All rights reserved.
//

import UIKit

typealias ServiceResponse = (NSDictionary?, NSString?) -> Void

func GetData(_ URL : String, Response : @escaping (NSDictionary?, NSString?) -> Void)
{
    let url = Foundation.URL (string:URL)
    let request: NSMutableURLRequest = NSMutableURLRequest()
    request.url = url
    request.httpMethod = "GET"
    request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
    let task = URLSession.shared.dataTask(with: request as URLRequest) {(data, response, error) in
        DispatchQueue.main.async(execute: {
            if(data != nil)
            {
                var json: NSDictionary = NSDictionary()
                do
                {
                    json = try JSONSerialization.jsonObject(with: data!, options: [JSONSerialization.ReadingOptions.mutableContainers, JSONSerialization.ReadingOptions.allowFragments]) as! NSDictionary
                }
                catch
                {
                    print("json error")
                    Response(nil, "json")
                }
                print(json)
                if(error == nil)
                {
                    let statuscode = (response as! HTTPURLResponse).statusCode
                    if statuscode == 200
                    {
                        Response(json,nil);
                        //return json
                    }
                    else if statuscode == 401 || statuscode == 403
                    {
                        //                        DeleteKeychain()
                        //                        accessToken{
                        //                            (status: Bool?) in
                        //                            if (status ?? false) == true
                        //                            {
                        //                                GetData(URL, Response: Response)
                        //                            }
                        //                        }
                        print("response 401 / 403")
                        Response(nil, "\(statuscode)" as NSString?)
                    }
                    else
                    {
                        print("status others \(statuscode)")
                        Response(nil, "\(statuscode)" as NSString?)
                    }
                }
                else
                {
                    print("error not nil \(error)")
                    Response(nil, "\(error!._code)" as NSString?)
                }
            }
            else
            {
                print("data nil")
                Response(nil,nil)
            }
        })
    }
    task.resume()
}

func PostData(_ URL : String, Params : String, Response : @escaping ServiceResponse)
{
    print(URL)
    let url = Foundation.URL(string:URL)
    let request : NSMutableURLRequest = NSMutableURLRequest()
    request.url = url
    request.httpMethod = "POST"
    request.httpBody = Params.data(using: String.Encoding.ascii)
    request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
    let task = URLSession.shared.dataTask(with: request as URLRequest){(data, response, error) in
        DispatchQueue.main.async(execute: {
            if(data != nil)
            {
                var json: NSDictionary = NSDictionary()
                do
                {
                    json = try JSONSerialization.jsonObject(with: data!, options: [JSONSerialization.ReadingOptions.mutableContainers, JSONSerialization.ReadingOptions.allowFragments]) as! NSDictionary
                }
                catch
                {
                    Response(nil, "json")
                }
                if(error == nil)
                {
                    let statuscode = (response as! HTTPURLResponse).statusCode
                    if statuscode == 200
                    {
                        Response(json,nil);
                        //return json
                    }
                    else if statuscode == 401 || statuscode == 403
                    {
                        //                        DeleteKeychain()
                        //                        accessToken{
                        //                            (status: Bool?) in
                        //                            if (status ?? false) == true
                        //                            {
                        //                                GetData(URL, Response: Response)
                        //                            }
                        //                        }
                        Response(nil, "\(statuscode)" as NSString?)
                    }
                    else
                    {
                        Response(nil, "\(statuscode)" as NSString?)
                    }
                }
                else
                {
                    Response(nil, "\(error!._code)" as NSString?)
                }
            }
            else
            {
                Response(nil,nil)
            }
        })
    }
    task.resume()
}

func PutData(_ URL : String, Params : String, Response : @escaping ServiceResponse)
{
    let url = Foundation.URL(string:URL)
    let request : NSMutableURLRequest = NSMutableURLRequest()
    request.url = url
    request.httpMethod = "PUT"
    request.httpBody = Params.data(using: String.Encoding.ascii)
    request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
    let task = URLSession.shared.dataTask(with: request as URLRequest){(data, response, error) in
        DispatchQueue.main.async(execute: {
            if(data != nil)
            {
                var json: NSDictionary = NSDictionary()
                do
                {
                    json = try JSONSerialization.jsonObject(with: data!, options: [JSONSerialization.ReadingOptions.mutableContainers, JSONSerialization.ReadingOptions.allowFragments]) as! NSDictionary
                }
                catch
                {
                    Response(nil, "json")
                }
                if(error == nil)
                {
                    let statuscode = (response as! HTTPURLResponse).statusCode
                    if statuscode == 200
                    {
                        Response(json,nil);
                        //return json
                    }
                    else if statuscode == 401 || statuscode == 403
                    {
                        //                        DeleteKeychain()
                        //                        accessToken{
                        //                            (status: Bool?) in
                        //                            if (status ?? false) == true
                        //                            {
                        //                                GetData(URL, Response: Response)
                        //                            }
                        //                        }
                        Response(nil, "\(statuscode)" as NSString?)
                    }
                    else
                    {
                        Response(nil, "\(statuscode)" as NSString?)
                    }
                }
                else
                {
                    Response(nil, "\(error!._code)" as NSString?)
                }
            }
            else
            {
                Response(nil,nil)
            }
        })
    }
    task.resume()
    
}

func DeleteData(_ URL : String, Response : @escaping ServiceResponse)
{
    let url = Foundation.URL (string:URL)
    let request: NSMutableURLRequest = NSMutableURLRequest()
    request.url = url
    request.httpMethod = "DELETE"
    request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
    
    let task = URLSession.shared.dataTask(with: request as URLRequest){(data, response, error) in
        DispatchQueue.main.async(execute: {
            if(data != nil)
            {
                var json: NSDictionary = NSDictionary()
                do
                {
                    json = try JSONSerialization.jsonObject(with: data!, options: [JSONSerialization.ReadingOptions.mutableContainers, JSONSerialization.ReadingOptions.allowFragments]) as! NSDictionary
                }
                catch
                {
                    print("json error")
                    Response(nil, "json")
                }
                if(error == nil)
                {
                    let statuscode = (response as! HTTPURLResponse).statusCode
                    if statuscode == 200
                    {
                        Response(json,nil);
                        //return json
                    }
                    else if statuscode == 401 || statuscode == 403
                    {
                        //                        DeleteKeychain()
                        //                        accessToken{
                        //                            (status: Bool?) in
                        //                            if (status ?? false) == true
                        //                            {
                        //                                GetData(URL, Response: Response)
                        //                            }
                        //                        }
                        print("response 401 / 403")
                        Response(nil, "\(statuscode)" as NSString?)
                    }
                    else
                    {
                        print("status others \(statuscode)")
                        Response(nil, "\(statuscode)" as NSString?)
                    }
                }
                else
                {
                    print("error not nil \(error)")
                    Response(nil, "\(error!._code)" as NSString?)
                }
            }
            else
            {
                print("data nil")
                Response(nil,nil)
            }
        })
    }
    task.resume()
}

func uploadImage(imageAsByte:NSData, url:String, Response : @escaping ServiceResponse){
    let imageType :String = imageTypeExtention(data: imageAsByte)
    if(imageType == "NSType")
    {
        Response(nil,"Format File Tidak Mendukung")
        //return false
    }
    let myUrl = NSURL(string:url)
    let request = NSMutableURLRequest()
    let boundary = generateBoundaryString()
    request.url = myUrl as URL?
    request.httpMethod = "POST"
    request.httpBody = createBodyWithParameters(filePathKey: "file", imageDataKey: imageAsByte, fileType: imageType, boundary: boundary) as Data
    request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
    //let task = session.uploadTaskWithRequest(request, fromData: imageAsByte)
    
    let task = URLSession.shared.dataTask(with: request as URLRequest){(data, response, error) in
        DispatchQueue.main.async(execute: {
            
            if(error == nil)
            {
                let statuscode = (response as! HTTPURLResponse).statusCode
                if(data != nil && statuscode == 200)
                {
                    var json: NSDictionary = NSDictionary()
                    do
                    {
                        json = try JSONSerialization.jsonObject(with: data!, options: [JSONSerialization.ReadingOptions.mutableContainers, JSONSerialization.ReadingOptions.allowFragments]) as! NSDictionary
                    }
                    catch
                    {
                        Response(nil, "json")
                    }
                    Response(json,nil);
                }
                else if statuscode == 401 || statuscode == 403
                {
//                    DeleteKeychain()
//                    accessToken{
//                        (status: Bool?) in
//                        if (status ?? false) == true
//                        {
//                            uploadImage(imageAsByte: imageAsByte, url: url, Response: Response)
//                        }
//                    }
                }
                else if statuscode == 500
                {
                    Response(nil, "500" as NSString?)
                }
                else
                {
                    Response(nil, "\(statuscode)" as NSString?)
                }
            }
            else
            {
                Response(nil, "\(error)" as NSString?)
            }
        })
    }
    task.resume()
}

func createBodyWithParameters(filePathKey: String?, imageDataKey: NSData, fileType: String, boundary: String) -> NSData {
    let body = NSMutableData()
    let filename = "image.\(fileType)"
    let mimetype = "image/\(fileType)"
    body.appendString("--\(boundary)\r\n")
    body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
    body.appendString("Content-Type: \(mimetype)\r\n\r\n")
    body.append(imageDataKey as Data)
    body.appendString("\r\n")
    body.appendString("--\(boundary)--\r\n")
    return body
}

func generateBoundaryString() -> String
{
    return "Boundary-\(NSUUID().uuidString)"
}

func imageTypeExtention(data: NSData) -> String {
    var c = [UInt32](repeating: 0, count: 1)
    data.getBytes(&c, length: 1)
    switch c[0] {
    case 0xFF:
        return "jpeg"
    case 0x89:
        return "png"
    default:
        return "NSType"
    }
}

