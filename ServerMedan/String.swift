//
//  String.swift
//  ServerMedan
//
//  Created by DickyChengg on 11/14/16.
//  Copyright © 2016 DickyChengg. All rights reserved.
//

import UIKit

extension String{
    
    func stringByRemovingAll(_ characters: [Character]) -> String {
        return String(self.characters.filter({ !characters.contains($0) }))
    }
    
    func stringByRemovingAll(_ subStrings: [String]) -> String {
        var resultString = self
        _ = subStrings.map { resultString = resultString.replace($0, "\n")}
        return resultString
    }
    
    var length: Int {
        return self.characters.count
    }
    
    var isEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    static func empty(_ value: String?) -> Bool
    {
        return (value == nil || value!.isEmpty) || (value!.trimmingCharacters(in: NSCharacterSet.whitespaces)).characters.count == 0
    }
    
    func between(_ min: Int, _ max: Int) -> Bool
    {
        return (self.length >= min && self.length <= max)
    }
    
    func replace(_ string: String, _ with: String) -> String
    {
        return self.replacingOccurrences(of: string, with: with)
    }
    
    func index(_ of: String) -> Int {
        let idx = range(of: of)?.lowerBound
        if idx == nil {
            return 0
        }
        return distance(from: startIndex, to: idx!)
    }
    
    func subStr(_ Start: Int, _ End: Int) -> String
    {
        let first = self.characters.index(
            self.startIndex,
            offsetBy: Start
        )
        
        let last = self.characters.index(
            self.endIndex,
            offsetBy: -1 * (self.length - End)
        )
        
        return self[Range(first..<last)]
    }
    
    func subStr(_ start: String, _ end: String, cut: Bool = false) -> String
    {
        var str = self.subStr(self.index(start), self.length)
        str = str.subStr(0, str.index(end))
        if cut {
            str = str.subStr(1, str.length)
        }
        return str
    }
}
