//
//  RegisterCls.swift
//  ServerMedan
//
//  Created by DickyChengg on 11/14/16.
//  Copyright © 2016 DickyChengg. All rights reserved.
//

import UIKit

class RegisterCls: DKeyboard {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var rePassword: UITextField!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        keyboard(scrollView: scrollView, hideKeyboard: true)
    }
    
    
    @IBAction func register(sender: AnyObject)
    {
        var messages: [String] = []
        if String.empty(firstName.text)
        {
            messages.append("Nama depan tidak boleh kosong")
        }
        if String.empty(email.text)
        {
            messages.append("Email tidak boleh kosong")
        }
        if !(email.text ?? "").isEmail
        {
            messages.append("Email yang anda masukkan tidak valid")
        }
        if String.empty(password.text) && (password.text ?? "").length < 6
        {
            messages.append("Kata sandi minimal 6 karakter")
        }
        if password.text != rePassword.text
        {
            messages.append("Kata sandi yang anda masukkan tidak cocok")
        }
        if messages.count != 0
        {
            self.Alert(message: messages[0], title: "Registrasi Gagal")
            return
        }
        self.showFader()
        PostData(API(.user) + "/register", Params: "fname=\(firstName.text!)&lname=\(lastName.text!)&email=\(email.text!)&password=\(password.text!)&rpassword=\(rePassword.text!)") {
            (response: NSDictionary?, error: NSString?) in
            print(response)
            if error == nil
            {
                if response?["status"] as? Bool ?? false
                {
                    self.Alert(
                        message: "Tekan tombol OK untuk melakukan login otomatis",
                        title: "Registrasi Berhasil",
                        actions:[
                            UIAlertAction(
                                title: "OK",
                                style: .default,
                                handler: {
                                    _ in
                                    print(response)
                                }
                            )
                        ]
                    )
                }
                else
                {
                    self.Alert(message: "", title: "Registrasi Gagal")
                }
            }
            else
            {
                self.Alert(message: "Gagal melakukan pendaftaran.\nCobalah beberapa saat lagi", title: "Registrasi Gagal")
            }
            self.hideFader()
        }
    }
    
    @IBAction func login(sender: AnyObject)
    {
        self.dismiss(animated: true, completion: nil)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextField = textField.superview?.viewWithTag(textField.tag + 1) as UIResponder!
        if nextField != nil {
            nextField!.becomeFirstResponder()
        }
        else {
            textField.resignFirstResponder()
        }
        
        if textField.returnKeyType == .go
        {
            //            login(textField)
        }
        return false
    }
}
