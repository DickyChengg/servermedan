//
//  Database.swift
//  ServerMedan
//
//  Created by DickyChengg on 11/17/16.
//  Copyright © 2016 DickyChengg. All rights reserved.
//

import UIKit
import FMDB

class Database {
    
    init(location: openFrom = .document)
    {
        self.location = location
    }
    
    enum openFrom
    {
        case bundle
        case document
    }
    
    private var database: FMDatabase!
    private var databaseName = "ServerMedan"
    private var location: openFrom = .document
    
    func Select(_ query: String, _ values:[AnyObject]? = nil) -> [NSDictionary]
    {
        open()
        var result:[NSDictionary] = []
        do
        {
            let res = try database.executeQuery(query, values: values)
            while(res.next())
            {
                result.append(res.resultDictionary() as NSDictionary)
            }
            res.close()
        }
        catch let error as NSError
        {
            print("Select Error = \(error)")
        }
        close()
        return result
    }
    
    func Insert(_ table: String, _ object:[String : String])
    {
        open()
        var query = "INSERT INTO \(table)("
        var values: [AnyObject] = []
        for (key,_) in object
        {
            query = query + "\(key), "
        }
        query = query.subStr(0, query.characters.count - 2)
        query = query + ") VALUES("
        for (_,value) in object
        {
            query = query + "?, "
            values.append(value as AnyObject)
        }
        query = query.subStr(0, query.characters.count - 2)
        query = query + ")"
        do
        {
            try database.executeUpdate(query, values: values)
        }
        catch let error as NSError
        {
            print("Insert error = \(error)")
        }
        close()
    }
    
    func Update(_ table: String, _ object:[String : String], _ condition: String)
    {
        open()
        var query = "UPDATE \(table) SET "
        var values: [AnyObject] = []
        for (key,value) in object
        {
            query = query + "\(key) = ?, "
            values.append(value as AnyObject)
        }
        
        // change the null or whitespace string to NSNull
        query = query.subStr(0, query.characters.count - 2)
        query = query + " WHERE \(condition)"
        do
        {
            try database.executeUpdate(query, values: values)
        }
        catch let error as NSError
        {
            print("Update error = \(error)")
        }
        close()
    }
    
    func Delete(_ table: String, _ condition: String? = nil, _ values:[AnyObject]?)
    {
        open()
        let query = "DELETE FROM \(table)" + (String.empty(condition) ? "WHERE\(condition!)" : "")
        do
        {
            try database.executeUpdate(query, values: values)
        }
        catch let error as NSError
        {
            print("Delete Error = \(error)")
        }
        close()
    }
    
    func numRows(_ table:String, _ condition: String? = nil, _ values:[AnyObject]? = []) -> Int
    {
        if !DModel.file.directory.exist("Sariputta.db")
        {
            return 0
        }
        return Select(
            "SELECT COUNT(*) AS rows FROM \(table) " + (!String.empty(condition) ? "WHERE \(condition!)" : ""),
            values
            )[0]["rows"] as? Int ?? 0
    }
    
    private func open()
    {
        switch(location)
        {
        case .bundle:
            database = FMDatabase(path: DModel.filePath.bundle(databaseName, ".db"))
            break
        case .document:
            database = FMDatabase(path: DModel.filePath.directory(databaseName + ".db"))
            break
        }
        if !database.open() {
            database.close()
            print("Unable to open database")
            return
        }
    }
    
    private func close()
    {
        database.close()
    }
}
