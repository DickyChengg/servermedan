//
//  LoginCls.swift
//  ServerMedan
//
//  Created by DickyChengg on 11/14/16.
//  Copyright © 2016 DickyChengg. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit

class LoginCls: DKeyboard, GIDSignInDelegate, GIDSignInUIDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        if !String.empty(DClass.email)
        {
            self.gotoMainMenu()
            return
        }
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        keyboard(scrollView: scrollView, hideKeyboard: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        initial(false, false)
    }
    
    @IBAction func forgotPassword(_ sender: AnyObject) {
        self.present(DSegue(storyboard: View.Account.Controll.rawValue, "forgotStr"), animated: true, completion: nil)
    }
    
    @IBAction func register(_ sender: AnyObject) {
        self.present(DSegue(storyboard: View.Account.Controll.rawValue, "registerStr"), animated: true, completion: nil)
    }
    
    @IBAction func login(_ sender: AnyObject) {
        var messages:[String] = []
        if String.empty(email.text)
        {
            messages.append("Email tidak boleh kosong")
        }
        if String.empty(password.text)
        {
            messages.append("Password tidak boleh kosong")
        }
        if !email.text!.isEmail
        {
            messages.append("Email yang anda masukkan tidak valid")
        }
        if messages != []
        {
            Alert(message: messages[0], title: "Login Gagal")
            return
        }
        self.showFader()
        PostData(API(.user) + "/login", Params:"email=\(email.text!)&password=\(password.text!)") {
            (response: NSDictionary?, error: NSString?) in
            print(response)
            if error == nil
            {
                if (response?["status"] as? Bool ?? false)
                {
                    let data = response?["data"] as? NSDictionary ?? NSDictionary()
                    DClass.saveKeychain(data: data)
                    self.gotoMainMenu()
                }
                else
                {
                    self.Alert(message: "Maaf login gagal", title: "Login Gagal")
                }
            }
            else
            {
                self.Alert(message: "Terdapat kesalahan dalam proses login.\nCobalah beberapa saat lagi", title: "Login Gagal")
            }
            self.hideFader()
        }
    }
    
    @IBAction func gmailLogin(_ sender: AnyObject) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func facebookLogin(_ sender: AnyObject) {
        let login : FBSDKLoginManager = FBSDKLoginManager()
        login.logIn(withReadPermissions: ["email"], from: self) { (result: FBSDKLoginManagerLoginResult?, error: Error?) in
            if(error == nil){
                let parameters = ["fields": "email, first_name, last_name, id, gender"]
                FBSDKGraphRequest(graphPath: "me", parameters: parameters).start(completionHandler: { (connection, user, requestError) -> Void in
                    if(requestError != nil)
                    {
                        print("request error facebook = \(requestError)")
                        return
                    }
                    
                    let User = user as? [String : AnyObject] ?? [String : AnyObject]()
                    
                    if !(User["id"] as? String ?? "").isEmail
                    {
                        self.Alert(message: "Email anda tidak valid", title: "Login gagal")
                    }
                    else
                    {
                        self.FacebookLogin(
                            id: User["id"] as? String ?? "",
                            email: User["email"] as? String ?? "",
                            fullname: (User["first_name"] as? String ?? "") + " " + (User["last_name"] as? String ?? ""),
                            gender: User ["gender"] as? String ?? ""
                        )
                    }
                })
            }
        }
    }
    
    
    
    // Google Login Controller
    func GoogleLogin(_ cover: String?, fullname name: String?, email: String?)
    {
        PostData(API(.google), Params: "cover=\(cover ?? "")&email=\(email ?? "")&fullname=\(name ?? "")&gender=1") {
            (response: NSDictionary?, error: NSString?) in
            if(error == nil)
            {
                if(response?["status"] as? Bool ?? false) == true
                {
                    DClass.saveKeychain(
                        data: response?["data"] as? NSDictionary ?? NSDictionary()
                    )
                    self.gotoMainMenu()
                }
                else
                {
                    self.Alert(message: "Maaf login gagal", title: "Login Gagal")
                    print(response ?? "response nil googleLogin")
                }
            }
            else
            {
                self.Alert(message: "Terdapat kesalahan dalam proses login.\nCobalah beberapa saat lagi", title: "Login Gagal")
                print(error ?? "error nil googleLogin")
            }
        }
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil)
        {
            GoogleLogin(
                user.profile.imageURL(withDimension: 200)?.absoluteString,
                fullname: user.profile.name,
                email: user.profile.email)
        }
        else
        {
            //dismissMenu(toRoot: true, message: "Terjadi kesalahan ketika melakukan login", status: false)
            print(error.localizedDescription)
        }
        GIDSignIn.sharedInstance().signOut()
    }

    
    // Facebook Login Controller
    
    func FacebookLogin(id: String, email: String , fullname name: String, gender: String)
    {
        if(String.empty(email))
        {
            //self.dismissMenu(toRoot: true, message: "Email anda tidak terdaftar", status: false)
            return
        }
        self.showFader()
        PostData(API(.facebook), Params: "id=\(id)&fullname=\(name)&email=\(email)&gender=\(gender)")
        {
            (response: NSDictionary?, error: NSString?) in
            if(error == nil)
            {
                if(response?["status"] as? Bool ?? false) == true
                {
                    DClass.saveKeychain(
                        data: response?["data"] as? NSDictionary ?? NSDictionary()
                    )
                    self.gotoMainMenu()
                }
                else
                {
                    self.Alert(message: "Maaf login gagal", title: "Login Gagal")
                    print(response ?? "response nil googleLogin")
                }
            }
            else
            {
                self.Alert(message: "Terdapat kesalahan dalam proses login.\nCobalah beberapa saat lagi", title: "Login Gagal")
                print(error ?? "error nil googleLogin")
            }
            self.hideFader()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextField = textField.superview?.viewWithTag(textField.tag + 1) as UIResponder!
        if nextField != nil {
            nextField!.becomeFirstResponder()
        }
        else {
            textField.resignFirstResponder()
        }
        
        if textField.returnKeyType == .go
        {
            login(textField)
        }
        return false
    }
    
    func gotoMainMenu()
    {
        self.present(
            self.DSegue(storyboard: View.Main.rawValue, "HomeStr"),
            animated: true,
            completion: nil
        )
    }
}
