//
//  UIImageView.swift
//  ServerMedan
//
//  Created by DickyChengg on 11/10/16.
//  Copyright © 2016 DickyChengg. All rights reserved.
//

import UIKit

extension UIImageView {

    func customImage(_ cornerRadius: CGFloat = 0, borderColor: UIColor? = nil, borderWidth: CGFloat? = 0)
    {
        self.layer.cornerRadius = cornerRadius
        
        if borderColor != nil {
            self.layer.borderColor = borderColor!.cgColor
            self.layer.borderWidth = borderWidth!
        }
    }
    
    private func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
            }.resume()
    }
    
    func setImage(_ link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
    
}
