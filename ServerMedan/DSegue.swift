//
//  DSegue.swift
//  ServerMedan
//
//  Created by DickyChengg on 1/8/17.
//  Copyright © 2017 DickyChengg. All rights reserved.
//

import UIKit

enum View : String
{
    case Display = "Display"
    case Main = "Main"
    enum Account : String
    {
        case Controll = "Control"
        case User = "User"
        case Member = "Member"
    }
    case Protery = "Proter"
}
