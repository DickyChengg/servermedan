//
//  UIView.swift
//  ServerMedan
//
//  Created by DickyChengg on 11/10/16.
//  Copyright © 2016 DickyChengg. All rights reserved.
//

import UIKit

extension UIView {
    
    func customView(_ cornerRadius: CGFloat = 8, borderColor: UIColor? = nil, borderWidth: CGFloat? = 0, shadowColor: UIColor? = nil, shadowOpacity: Float? = 0.5, shadowRadius: CGFloat? = 5)
    {
        self.layer.cornerRadius = cornerRadius
        
        if borderColor != nil {
            self.layer.borderColor = borderColor!.cgColor
            self.layer.borderWidth = borderWidth!
        }
        
        if shadowColor != nil {
            self.layer.shadowColor = shadowColor!.cgColor
            self.layer.opacity = shadowOpacity!
            self.layer.shadowRadius = shadowRadius!
        }
    }
    
}
