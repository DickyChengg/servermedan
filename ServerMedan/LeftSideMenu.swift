//
//  LeftSideMenu.swift
//  ServerMedan
//
//  Created by DickyChengg on 11/14/16.
//  Copyright © 2016 DickyChengg. All rights reserved.
//

import UIKit

class LeftSideMenu: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func dismissMenu(toRoot: Bool = false, message: String? = nil, status: Bool = true)
    {
        UIView.animate(
            withDuration: 0.4,
            delay: 0,
            options: .curveEaseInOut,
            animations: {
                self.view.frame.origin.x = -screen.width
            },
            completion: {
                finished in
                if toRoot {
                    _ = DClass.controller.navigationController?.popToRootViewController(animated: false)
//                    if message != nil
//                    {
//                        DClass.controller.message(message!, status: status)
//                    }
                }
                self.dismiss(animated: false, completion: nil)
            }
        )
        hideFader()
    }

}
