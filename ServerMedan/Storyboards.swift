//
//  Storyboards.swift
//  ServerMedan
//
//  Created by DickyChengg on 12/18/16.
//  Copyright © 2016 DickyChengg. All rights reserved.
//

import UIKit

internal enum Board : String
{
    case Display = "Display"
    case Main = "Main"
    enum Account : String
    {
        case Controll = "Controll"
        case User = "User"
        case Member = "Member"
    }
    case Proter = "Proter"
    case SideMenu = "Side"
}
